# webpack 自动编译 Ts

# 一、新建 index.html 页面

- 使用 `mkdir` 命令来创建 `public 目录`，然后再使用 `touch` 命令创建 `index.html 文件`。示例如下：

```bash
mkdir public
touch public/index.html
```

- 这将先创建一个名为 `public 的目录`，然后在该目录下创建 `index.html 文件`。现在你就可以在 `public/index.html` 文件中编写 `HTML` 代码了。

- 接下来，在 `public/index.html` 文件中编写 基本的 的代码。

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>webpack & TS</title>
  </head>
  <body></body>
</html>
```

# 二、新建入口文件 TS

- 使用 `mkdir` 命令来创建 `src 目录`，然后再使用 `touch` 命令创建 `main.ts 文件`。示例如下：

```bash
mkdir src
touch src/main.ts
```

- 这将先创建一个名为 `src 的目录`，然后在该目录下创建 `main.ts 文件`。现在你就可以在 `src/main.ts` 文件中编写 `TypeScript` 代码了。

- 接下来，在 `src/main.ts` 文件中编写 基本的 的代码。

```js
document.write('hello Webpack TS1!');
let arr: number = 100;
console.log('arr: ', arr);
```

# 三、安装依赖

- 首先，在项目的 `根` 目录，然后使用 `npm init -y` 命令初始化 `package.json` 文件。

```bash
npm init -y
```

- 接下来，使用 `npm install` 命令安装 `webpack`、`webpack-cli`、`typescript`、`ts-loader` 等相关依赖。

```js
npm i -D typescript
npm i -D webpack@4.41.5 webpack-cli@3.3.10
npm i -D webpack-dev-server@3.10.2
npm i -D html-webpack-plugin@4.5.0  clean-webpack-plugin@3.0.0
npm i -D ts-loader@8.0.17  -D cross-env@7.0.3
```

# 四、配置 webpack.config.js

- 首先，在项目的 `根` 目录，使用 `mkdir` 命令创建 `build` 文件夹， 然后在该目录下使用 `touch` 命令创建 `webpack.config.js 文件`。

```bash
mkdir build
touch build/webpack.config.js
```

- 接下来，在 `webpack.config.js` 文件中编写 `webpack` 的配置信息。

```js
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const isProd = process.env.NODE_ENV === 'production'; // 是否生产环境

function resolve(dir) {
  return path.resolve(__dirname, '..', dir);
}

module.exports = {
  mode: isProd ? 'production' : 'development',
  entry: {
    app: './src/min.ts',
  },

  output: {
    path: resolve('dist'),
    filename: '[name].[contenthash:8].js',
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        include: [resolve('src')],
      },
    ],
  },

  plugins: [
    new CleanWebpackPlugin({}),

    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
  ],

  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },

  devtool: isProd ? 'cheap-module-source-map' : 'cheap-module-eval-source-map',

  devServer: {
    host: 'localhost', // 主机名
    stats: 'errors-only', // 打包日志输出输出错误信息
    port: 8083,
    open: true,
  },
};
```

# 五、配置 tsconfig.json

- 首先，在项目的 `根` 目录，使用 `touch` 命令创建 `tsconfig.json 文件`。

```bash
touch tsconfig.json
```

- 接下来，在 `tsconfig.json` 文件中编写 `TypeScript` 的配置信息。

```js
{
  "compileOnSave": true, // 保存时自动编译
  "compilerOptions": {
    "target": "es5",
    "module": "commonjs", // 模块化规范
    "strict": false, // 严格模式
  }
}
```

# 六、在 package.json 配置启动命令

- 接下来，在 `package.json` 文件中编写 `scripts` 命令的配置信息。

```js
  "scripts": {
    "dev": "cross-env NODE_ENV=development webpack-dev-server --config build/webpack.config.js",
    "build": "cross-env NODE_ENV=production webpack --config build/webpack.config.js"
  },
```

# 七、启动项目

- 接下来，终端启动在 `package.json` 文件中 `scripts` 命令的配置信息。

```bash
npm run dev
```

- 接下来，在浏览器中访问 `http://localhost:8083` 即可看到效果。
  ![image.png](https://image.myxuechao.com/TS/1.png)

# 八、效果图

![image.png](https://image.myxuechao.com/TS/2.png)
![image.png](https://image.myxuechao.com/TS/3.png)

# 九、总结

- 以上，就是使用 `webpack` 自动编译 `TypeScript` 的过程。
- 接下来，我们就可以在 `src/main.ts` 文件中编写 `TypeScript` 代码了。
- 它将自动编译成 `JavaScript` 代码，并自动刷新浏览器。

# 十、项目地址

- [项目地址](https://gitee.com/lxc482113948/ts_webpack_auto-compile.git)

# 参考文档

- [webpack 中文网](https://www.webpackjs.com/)
- [TypeScript 中文网](https://www.tslang.cn/)
- [TypeScript 入门教程](https://ts.xcatliu.com/)
- [TypeScript 官方文档](https://www.typescriptlang.org/docs/handbook/basic-types.html)
